import spacy
from spacy import displacy
import nltk
import unicodedata
from nltk.tokenize.toktok import ToktokTokenizer
import pandas as pd
import re

#Install Dutch Spacy with python -m spacy download nl_core_news_sm
#import nl_core_news_sm

# Execute Applications/python 3.7/Install Certificates.command
# Downlaod stopwords nltk.download()

stopword_list = nltk.corpus.stopwords.words('dutch')
df = pd.read_excel('cleaned_data.xlsx')


def remove_accented_chars(text):
    text = unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode('utf-8', 'ignore')
    return text


def remove_special_characters(text, remove_digits=False):
    pattern = r'[^a-zA-z0-9\s]' if not remove_digits else r'[^a-zA-z\s]'
    text = re.sub(pattern, '', text)
    return text

def remove_stopwords(text, is_lower_case=False):
    tokens = tokenizer.tokenize(text)
    tokens = [token.strip() for token in tokens]

    if is_lower_case:
        filtered_tokens = [token for token in tokens if token not in stopword_list]
    else:
        filtered_tokens = [token for token in tokens if token.lower() not in stopword_list]

    filtered_text = ' '.join(filtered_tokens)
    return filtered_text


def normaliseren_tekst(corpus, accented_char_removal=True, special_char_removal=True,stopword_removal=True,
                       remove_digits=True, text_lower_case=True):

    genormaliseerde_tekst = []

    for doc in corpus:
    # vervang karakters met accenten

        if accented_char_removal:
            doc = remove_accented_chars(doc)

        # verwijder speciale karakters
        if special_char_removal:
            # insert spaces between special characters to isolate them
            special_char_pattern = re.compile(r'([{.(-)!}])')
            doc = special_char_pattern.sub(" \\1 ", doc)
            doc = remove_special_characters(doc, remove_digits=remove_digits)

        # verwijder stopwoorden
        if stopword_removal:
            # doc = remove_stopwords(doc.decode('unicode-escape'), is_lower_case=text_lower_case)
              doc = remove_stopwords(doc, is_lower_case=text_lower_case)

    genormaliseerde_tekst.append(doc)

    return genormaliseerde_tekst

from gensim.test.utils import common_texts
from gensim.corpora.dictionary import Dictionary
from gensim.models import ldamodel


normalized_text = normaliseren_tekst(t.summary_clean.values)

# Create a corpus from a list of texts
dictionary = Dictionary(normalized_text.split())
corpus = [dictionary.doc2bow(text) for text in normalized_text]

# Train the model on the corpus.
lda = ldamodel(corpus, num_topics=100)